# MYSQL
sudo docker run --name project-mysql -e MYSQL_ROOT_PASSWORD=PASSWORD -e MYSQL_DATABASE=dbname -e MYSQL_USER=manager -e MYSQL_PASSWORD=MANAGER -d mysql:5.7


# DJANGO
- mkdir api-project && cd api-project
- virtualenv -p python3 env && source env/bin/activate
- pip3 install django djangorestframework djangorestframework-api-key django-rest-auth django-allauth mysqlclient
- django-admin startproject project .
- cd project && django-admin startapp appname && cd ..

## settings.py
- ALLOWED_HOSTS = ['*']

- INSTALLED_APPS = [
    'django.contrib.sites',
    'project.appname',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_api_key',
    'rest_auth',
    'allauth',
    'allauth.account',
    'rest_auth.registration',
]

- SITE_ID = 1

- EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

- DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dbname',
        'USER': 'manager',
        'PASSWORD': 'MANAGER',
        'HOST': '172.17.0.2', # Or an IP Address that your DB is hosted on
        'PORT': '',
    },
}

- REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework_api_key.permissions.HasAPIKey',
        'rest_framework.permissions.IsAuthenticated',
    ],
}

- API_KEY_CUSTOM_HEADER = "HTTP_X_API_KEY"

## MIGRATE AND CREATE SUPERUSER
- python3 manage.py migrate
- python3 manage.py createsuperuser --email=mateo.zeas@insac.net --username=admin

## ADD URLS
- from django.urls import path, include
- path('rest-auth/', include('rest_auth.urls')),
- path('rest-auth/registration/', include('rest_auth.registration.urls'))

## RUN SERVER
- python3 manage.py runserver

## CREATE USER AND API-KEY
- API-KEY: zkYRIffh.n1FJj2PVMPQS9Z1tixd45dZdztCGK7aO

# POSTMAN
## LOGIN
- http://127.0.0.1:8000/rest-auth/login/
- {	"username":"mateozeas99","password": "password2019"}
## LOGPUT
- http://127.0.0.1:8000/rest-auth/logout/
- Authorization: Token xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# APOLLO
## NODEENV
- sudo pip3 install nodeenv
## PROJECT
- mkdir apollo-project && cd apollo-project
- nodeenv env
- . env/bin/activate
## NPM
- npm init -y
- npm config list
- npm set init.author.name "Mateo Zeas"
- npm set init.author.email "mateo.zeas@insac.net"
- npm set init.author.url "mateozeas.com"
- npm set init.license "GNU"
## MOST NPM (optional)
- npm install nodemon --save-dev
- npm install apollo-server apollo-server-express express graphql apollo-datasource-rest cors --save
## PACKAGE JSON
- "start": "nodemon src/index.js",
## index.js
- mkdir src && touch src/index.js
>>>
const express = require('express') 
const { ApolloServer } = require ('apollo-server-express'); 
const cors = require('cors'); 
const typeDefs = require('./typeDefs'); 
const resolvers = require('./resolvers');

const Rest_Auth_Api = require('./datasources/rest_auth_api');

const app = express(); // your express configuration here

app.use(cors());

const server = new ApolloServer({ 
    typeDefs, 
    resolvers, 
    dataSources: () => { 
        return { rest_Auth_Api: new Rest_Auth_Api(), } 
    }, 
    context: ({req, res})=> ({req, res}) });

server.applyMiddleware({ app, path: '/graphql' });

app.listen({ port: 8080 }, () => { console.log('Apollo Server on http://localhost:8080/graphql'); });
>>>

## CREATE TYPEDEFS, RESOLVERS AND DATASOURCES
- mkdir src/typeDefs && mkdir src/resolvers && mkdir src/datasources
- touch src/typeDefs/index.js && touch src/resolvers/index.js && touch src/datasources/rest_auth_api.js

## typeDefs/index.js
>>>
const { gql } = require('apollo-server-express');

const tyfeDefs = gql` 
    type Query{
        login(user_email:String!, password:String!): Token
        logout: Message
        registration(username:String!, email:String!, password1:String!, password2:String!): Token
    }
    type Token {
        key: String
    }
    type Message {
        detail: String
    }
`;

module.exports = tyfeDefs;
>>>

## resolvers/index.js
>>>
const resolvers = {
    Query: {
        login: async(_source,{user_email,password},{dataSources}) => {
            return dataSources.rest_Auth_Api.login(user_email,password);
        },
        logout: async(_source,_args,{dataSources}) => {
            return dataSources.rest_Auth_Api.logout();
        },
        registration: async(_source,{username,email, password1, password2},{dataSources})=> {
            return dataSources.rest_Auth_Api.registration(username,email, password1, password2);
        }
    },
};

    module.exports = resolvers;
>>>
## datasources/rest_auth_api.js
>>>
const { RESTDataSource } = require('apollo-datasource-rest');

class rest_auth_api extends RESTDataSource {
    constructor(){
        super();
        this.baseURL = 'http://127.0.0.1:8000/rest-auth/'
    }

    async login(user_email,password){
        let input = {username: user_email, password: password}
        try {
            let response = await this.post('login/',JSON.stringify(input),{
                headers: this.context.req.headers,
            });
            return response;
        } catch (error) {
            console.log(error);
            return error;
        }
    }

    async logout(){
        try {
            return await this.post('logout/',{},{
                headers: this.context.req.headers,
            });
        } catch (error) {
            console.log(error);
        }
    }

    async registration(username,email, password1, password2){
        let input = {username:username,email:email, password1:password1, password2:password2}
        return await this.post('registration/',JSON.stringify(input),{
            headers: this.context.req.headers,
        });
    }
}
module.exports = rest_auth_api;
>>>
# ANGULAR
## CREATE PROJECT
ng new angular-project
cd angular-project
ng serve

## CREATE 
ng g c login

# CHANGE ROUTE
