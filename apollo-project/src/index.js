const express = require('express') 
const { ApolloServer } = require ('apollo-server-express'); 
const cors = require('cors'); 
const typeDefs = require('./typeDefs'); 
const resolvers = require('./resolvers');

const Rest_Auth_Api = require('./datasources/rest_auth_api');

const app = express(); // your express configuration here

app.use(cors());

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
        return {
            rest_Auth_Api: new Rest_Auth_Api(),
        }
    },
    context: ({req, res})=> ({req, res})
});

server.applyMiddleware({ app, path: '/graphql' });

app.listen({ port: 8080 }, () => { console.log('Apollo Server on http://localhost:8080/graphql'); });