const { RESTDataSource } = require('apollo-datasource-rest');

class rest_auth_api extends RESTDataSource {
    constructor(){
        super();
        this.baseURL = 'http://127.0.0.1:8000/rest-auth/'
    }

    async login(user_email,password){
        let input = {username: user_email, password: password}
        try {
            let response = await this.post('login/',JSON.stringify(input),{
                headers: this.context.req.headers,
            });
            return response;
        } catch (error) {
            console.log(error);
            return error;
        }
    }

    async logout(){
        try {
            return await this.post('logout/',{},{
                headers: this.context.req.headers,
            });
        } catch (error) {
            console.log(error);
        }
    }

    async registration(username,email, password1, password2){
        let input = {username:username,email:email, password1:password1, password2:password2}
        return await this.post('registration/',JSON.stringify(input),{
            headers: this.context.req.headers,
        });
    }
}
module.exports = rest_auth_api;