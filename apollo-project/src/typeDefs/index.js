const { gql } = require('apollo-server-express');

const tyfeDefs = gql`
    type Query{
        login(user_email:String!, password:String!): Token
        logout: Message
        registration(username:String!, email:String!, password1:String!, password2:String!): Token
    }
    type Token {
        key: String
    }
    type Message {
        detail: String
    }
`;

module.exports = tyfeDefs;