const resolvers = {
    Query: {
        login: async(_source,{user_email,password},{dataSources}) => {
            return dataSources.rest_Auth_Api.login(user_email,password);
        },
        logout: async(_source,_args,{dataSources}) => {
            return dataSources.rest_Auth_Api.logout();
        },
        registration: async(_source,{username,email, password1, password2},{dataSources})=> {
            return dataSources.rest_Auth_Api.registration(username,email, password1, password2);
        },
    },
};

    module.exports = resolvers;